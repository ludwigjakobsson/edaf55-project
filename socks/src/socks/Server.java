package socks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Server extends Thread{
	   private ServerSocket serverSocket;

	public Server(int port) throws IOException {
		serverSocket = new ServerSocket(port);
	    serverSocket.setSoTimeout(20000);
	}
	
	public void run() {
	      while(true) {
	         try {
	            System.out.println(">> Waiting for client on port " + serverSocket.getLocalPort() + "...");
	            Socket server = serverSocket.accept(); 
	            
	            System.out.println(">> Just connected to " + server.getRemoteSocketAddress());
	            
	            
	            DataInputStream in = new DataInputStream(server.getInputStream());
	           // System.out.println("<< " + in.readUTF());
	            int length = in.readInt();
        			

	            if(length > 0 ) {
	            		byte[] message = new byte[length];
	            		in.readFully(message, 0, message.length);
	            		for(int i = 0 ; i < length ; i++) 
	            			System.out.println("test: " + (char)message[i]);
	            		int i = message[0];
	            		char a = (char) (message[1]);
	            		
	            }
	            
	            
	            DataOutputStream out = new DataOutputStream(server.getOutputStream());
	            //out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress() + "\nGoodbye!");
	            Path path = Paths.get("/Users/ludde/repos/edaf55-project/socks/src/socks/dog.JPG");
	            byte[] data = Files.readAllBytes(path);
		        
	            
	            out.writeInt(data.length);
	            out.write(data);
	            
	            
	            
	            
	            
	            server.close();
	            
	         } catch (SocketTimeoutException s) {
	            System.out.println("Socket timed out!");
	            break;
	         } catch (IOException e) {
	            e.printStackTrace();
	            break;
	         }
	      }
	   }

	public static void main(String[] args) {
		
		      int port = 51715;
		      try {
		         Thread t = new Server(port);
		         t.start();
		         
		      } catch (IOException e) {
		         e.printStackTrace();
		      }
		   }

}
