package socks;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client2 {
	static Socket clientSocket;

	public static void main(String[] args) {
		int port = 9999;
		String serverName = "localhost";
		try {
			System.out.println(">> Connecting to " + serverName + " on port " + port);
			clientSocket = new Socket(serverName, port);
			System.out.println(">> Just connected to " + clientSocket.getRemoteSocketAddress());
			
			
			Scanner scan = new Scanner(System.in);
			String inp = "hej";
			
			while(inp != "q") {
				
				getImage();
				inp = scan.nextLine();
			}
			scan.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void getImage() throws IOException {
		OutputStream os = clientSocket.getOutputStream();
		InputStream in = clientSocket.getInputStream();
		// Write
		
		String datatest = "GET";
		byte[] message = datatest.getBytes();
		
		try {
			os.write(message);
			os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Mesage sent");

		// Read
		
		int headerLength = 24;
		System.out.println("Reading Header... ");

		byte[] headerData = readBytes(in, headerLength);
		String[] header = new String(headerData).split(" ");

		 for (int i = 0; i< headerLength ; i++) {
		 System.out.print((char)headerData[i]);
		 }
		 System.out.println();

		int imageSize = Integer.parseInt(header[0]);
		long timestamp = Long.parseLong(header[1]);
		int mode = Integer.parseInt(header[2]);

		System.out.println("Image Size: " + imageSize);
		System.out.println("Timestamp: " + timestamp);
		System.out.println("Mode: " + mode);

		// Read Image
		byte[] imageData = readBytes(in, imageSize);
		
		FileOutputStream fos = new FileOutputStream(
				"/h/d5/b/dat14lja/Desktop/edaf55/edaf55-project/socks/src/test.jpg");
		fos.write(imageData);
		fos.close();
	}

	private static byte[] readBytes(InputStream is, int size) throws IOException {
		byte[] data = new byte[size];
		
//		int remaining = size;
//		int offset = 0;
//		while (remaining > 0) {
//			int n = is.read(data, offset, remaining);
//			offset += n;
//			remaining -= n;
//
//		}
		
		int n;
		int bytesread = 0;
		while((n = is.read(data, bytesread, size-bytesread)) > 0) 
			bytesread += n;
		
		return data;
	}

}