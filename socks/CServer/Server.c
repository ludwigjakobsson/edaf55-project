#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>

int main() {


        char str[100];
        int listen_fd, comm_fd;
        printf("Starting\n");
        struct sockaddr_in servaddr;

        listen_fd = socket(AF_INET, SOCK_STREAM, 0);

        bzero(&servaddr, sizeof(servaddr));

        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = htons(INADDR_ANY);
        servaddr.sin_port = htons(41237);

        bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));

        listen(listen_fd, 10); // 10 = nbr of allowed connections

        comm_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL);
        printf("%s\n", "Connected!\n");
        // Echo messages
        // while(1) {
        //
        //         bzero(str, 100);
        //
        //         int n = read(comm_fd, str, 100);
        //         if ( n < 0 ) {
        //                 perror("ERROR reading from socket");
        //
        //         }
        //
        //
        //
        //         printf("Echoing back - %s", str);
        //
        //         write(comm_fd, str, strlen(str) + 1);
        //
        //
        //
        // }

        //Send Images v1
        // printf("%s\n", "Getting picture size");
        // FILE *picture;
        // picture = fopen("/Users/ludde/repos/edaf55-project/socks/CServer/dog.JPG", "r");
        // int size;
        // fseek(picture, 0, SEEK_END);
        // size = ftell(picture);
        // fseek(picture, 0, SEEK_SET);
        //
        // // Send Picture Size
        // sprintf(str, "%d", size);
        // printf("%s%d\n", "Sending picture size: ", size);
        // write(comm_fd, str, strlen(str) + 1);
        // printf("%s\n", "Sent!");
        //
        //
        // //Send Picture
        // printf("%s\n", "Sending picture as byte array");
        // char send_buffer[size];
        // printf("%s\n","pic sent" );
        //
        // while(!feof(picture)) {
        //         fread(send_buffer, 1 , sizeof(send_buffer), picture);
        //         write(comm_fd, send_buffer, sizeof(send_buffer));
        //         bzero(send_buffer, sizeof(send_buffer));
        //
        // }


        //Send Images v2
        printf("%s\n", "Getting picture size");
        FILE *picture;
        picture = fopen("/Users/ludde/repos/edaf55-project/socks/CServer/dog.JPG", "r");
        int size;
        fseek(picture, 0, SEEK_END);
        size = ftell(picture);
        fseek(picture, 0, SEEK_SET);

        // Send Picture Size
        printf("%s%d\n", "Sending picture size: ", size);
        write(comm_fd, &size, sizeof(size));
        printf("%s\n", "Sent!");


        //Send Picture
        printf("%s\n", "Sending picture as byte array");
        char send_buffer[size];

        while(!feof(picture)) {
                fread(send_buffer, 1 , sizeof(send_buffer), picture);
                write(comm_fd, send_buffer, sizeof(send_buffer));
                bzero(send_buffer, sizeof(send_buffer));

        }
        printf("%s\n","pic sent" );

}
