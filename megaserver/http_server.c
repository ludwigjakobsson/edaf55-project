#include "server_common.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>  // for memcpy, memset, strlen
#include <poll.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/times.h>
#include <math.h>

#define USE_CAMERA

// some diagnostic printouts
// #define INFO
//#define DEBUG
// more diagnostic printouts
// #undef DEBUG

// the strncasecmp function is not in the ISO C standard
// #define USE_POSIX_FUNCTION

#ifdef USE_CAMERA
#include "camera.h"
#endif


#define MOTION_PORT 9090


#define BUFSIZE 50000
struct client{
    int  connfd;
    int  sendImgs;
    int mode;
    int alive;
    byte sendBuff[BUFSIZE];
#ifdef USE_CAMERA
    camera* cam;
    byte* frame_data;
#endif
};

struct global_state {
    int listenfd;
    int running;
    int quit;
    pthread_t main_thread;
    pthread_t write_thread;
    pthread_t read_thread;
    pthread_t ui_thread;
    pthread_t bg_thread;
    struct pollfd pfd;
    int motionfd;
#ifdef USE_CAMERA
    camera* cam;
#endif
};


int client_write_string(struct client* client);
int client_write_n(struct client* client, size_t n);
void* serve_client(void *ctxt);
void server_quit(struct global_state* s);
void signal_to_bg_task();
int is_running(struct global_state* state);

/////////////// camera stuff

#define ERR_OPEN_STREAM 1
#define ERR_GET_FRAME 2

struct client;

static pthread_mutex_t global_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t global_cond = PTHREAD_COND_INITIALIZER;

#ifdef USE_CAMERA

/* try to open capture interface.
 * returns 0 on success
 */
int try_open_camera(struct global_state* state)
{
    state->cam = camera_open();
    if (!state->cam){ // Check if null
        printf("axism3006v: Stream is null, can't connect to camera");
        return ERR_OPEN_STREAM;
    }
    return 0;
}

void close_camera(struct global_state* state)
{
    if(state->cam) {
	camera_close(state->cam);
	state->cam = NULL;
    }
}


/* Sets up the packet structure in client->sendBuff.
 * This is a minimal HTTP header for an image/jpeg
 *
 * sets client->frame_data to the beginning of the frame
 * in the packet
 *
 * returns size of entire packet, or -1 if frame too large
 */
ssize_t setup_packet(struct client* client, uint32_t frame_sz, frame* fr)
{

    // Get Header info
    size_t header_size;
    struct timeval timestamp;
    gettimeofday(&timestamp, NULL);
    //printf("Time : %ld.%7ld\n", (long int)timestamp.tv_sec, (long int)timestamp.tv_usec);
    //unsigned long long time_in_micros = 1000000 * timestamp.tv_sec + timestamp.tv_usec;
    // printf("Time : %16lld\n", time_in_micros);
    // printf("Time : %lld\n", time_in_micros);
    int32_t sec =  timestamp.tv_sec;
    int32_t msec =  timestamp.tv_usec;
    printf("Time : %d.%d\n", sec, msec);

    char mode = 0;
    #ifndef MOTION_PORT
    printf("%s\n", "Mode Active");
    mode = get_frame_motion(fr);
    #endif
    printf("Mode: %1d\n", mode);

    // Send to Client
    snprintf(client->sendBuff, sizeof(client->sendBuff),
            "%5zu#%d#%7d#%d", frame_sz, sec, msec, mode);
    header_size = strlen(client->sendBuff);



    if(header_size + frame_sz > sizeof(client->sendBuff)) {
      return -1;
    }
    client->frame_data = client->sendBuff + header_size;
#ifdef DEBUG
    printf("Header size = " FORMAT_FOR_SIZE_T "\n", header_size);
#endif
    return header_size+frame_sz;
}

/* send packet with frame
 * returns 0 on success
 */
int client_send_frame(struct client* client, frame* fr)
{

    // this should really be a compile-time check, but that is complicated
#ifndef DISABLE_SANITY_CHECKS
    if(sizeof(size_t) != sizeof(uint32_t)) {
        printf("sizeof(size_t)=%d, sizeof(uint32_t)=%d\n", sizeof(size_t), sizeof(uint32_t));
        printf("Not sending frame, size sanity check failed\n");
        return 2;
    }
#endif

    size_t frame_sz = get_frame_size(fr);
    byte* data = get_frame_bytes(fr);
    int result;

    ssize_t packet_sz = setup_packet(client, frame_sz, fr);

    if(packet_sz < 0) {
        printf("Frame too big for send buffer(" FORMAT_FOR_SIZE_T " > " FORMAT_FOR_SIZE_T "), skipping.\n", frame_sz, sizeof(client->sendBuff));
        result = 1;
    } else {
        int written;
#ifdef DEBUG
        printf("encode size:" FORMAT_FOR_SIZE_T "\n",  frame_sz);
        printf("sizeof(size_t)=" FORMAT_FOR_SIZE_T ", sizeof(uint32_t)=" FORMAT_FOR_SIZE_T "\n", sizeof(size_t), sizeof(uint32_t));
#endif
        memcpy(client->frame_data, data, frame_sz);

        written=client_write_n(client, packet_sz);
        if(written != packet_sz) {
          printf("WARNING! packet_sz=" FORMAT_FOR_SIZE_T ", written=%d\n", packet_sz, written);
          result = 3;
        } else {
          result = 0;
        }
    }

    return result;
}

/* get frame from camera and send to client
 * returns zero on success
 */
int try_get_frame(struct client* client)
{
    int result=-1;
    frame *fr = fr = camera_get_frame(client->cam);

    if(fr) {
        if((result = client_send_frame(client, fr))) {
          printf("Warning: client_send_frame returned %d\n", result);
        }
        frame_free(fr);
    } else {
        return ERR_GET_FRAME;
    }
    return result;
}
#endif

////////////// Server stuff

int client_write_string(struct client* client)
{
  return write_string(client->connfd, client->sendBuff);
}

/*
 * Returns number of bytes written.
 * Returns -1 if an error occurs and sets errno.
 * Note: an error occurs if the web browser closes the connection
 *   (might happen when reloading too frequently)
 */
int client_write_n(struct client* client, size_t n)
{
    return write_n(client->connfd, client->sendBuff,n);
}


/* A minimal, not-even-close-to-compliant HTTP request parser.
 * Only accepts GET / and expects *exactly* one space char between GET and /
 */
int parse_http_request(const char* buf, size_t sz)
{
  if(strncmp(buf, "GET", 4) == 0) {
    return 2;
  } else if (strncmp(buf, "IDLE", 4) == 0) {
    return 0;
  } else if (strncmp(buf, "MOVIE", 5) == 0) {
    return 1;
  } else if (strncmp(buf, "SHUTDOWN", 8) == 0) {
    return 3;
  }

  return -1;
}

static void send_internal_error(struct client* client,const char* msg)
{
    snprintf(client->sendBuff, sizeof(client->sendBuff),
	     "HTTP/1.0 500 Internal Error\nContent-Length: " FORMAT_FOR_SIZE_T "\nContent-Type: text/plain\n\n%s",
	     strlen(msg), msg);
    client_write_string(client);
}

void* read_client(void *ctxt) {


    struct client* client = ctxt;
    //
    // int rres = read(client->connfd, buf, 1024);
    // if(rres < 0) {
    //      perror("motion_task: read");
	  //      return (void*) (intptr_t) errno;
    // }
    // #ifdef DEBUG
    //     printf("read: rres = %d\n", rres);
    //     printf("buf[%s]\n", buf);
    // #endif
    // int hres = parse_http_request(buf, 1024);
    // printf("Message %d\n", hres);
    int rres;
    int hres;
    pthread_mutex_lock(&global_mutex);
    int alive = client->alive;
    pthread_mutex_unlock(&global_mutex);
    printf("Hello I'm not alive!");
    while(alive) {
      char buf[1024] = {};
      printf("Waiting for input");
      rres = read(client->connfd, buf, 1024);

      hres = parse_http_request(buf, 1024);
      printf("Here > %d\n", hres);
      if (hres == 0 || hres == 1) { // IDlE OR MOVIEMODE
        pthread_mutex_lock(&global_mutex);
        client->mode = hres;
        pthread_mutex_unlock(&global_mutex);
      } else if(hres == 3){    // SHUTDOWN
        pthread_mutex_lock(&global_mutex);
        client->alive = 0;
        pthread_mutex_unlock(&global_mutex);
      }
      pthread_mutex_lock(&global_mutex);
      alive = client->alive;
      pthread_mutex_unlock(&global_mutex);

    }

    return (void*) (intptr_t) close(client->connfd);
}





/* The function for a thread that processes a single client request.
 * The client struct pointer is passed as void* due to the pthreads API
*/
void* serve_client(void *ctxt) //TODO while loop this + idle mode is sleep(5);
{

    struct client* client = ctxt;
    memset(client->sendBuff, 0, sizeof(client->sendBuff));
    pthread_mutex_lock(&global_mutex);
    int alive = client->alive;
    pthread_mutex_unlock(&global_mutex);

    int mode;
    while(alive) {
      pthread_mutex_lock(&global_mutex);
      mode = client->mode;
      pthread_mutex_unlock(&global_mutex);
      if(mode==0) {
        unsigned int retTime = time(0) + 5;   // Get finishing time.
        while (time(0) < retTime);
      }

      int cres = 0;
            if( !client->cam || (cres=try_get_frame(client))) {
                  printf("ERROR getting frame from camera: %d\n",cres);
  	              send_internal_error(client, "Error getting frame from camera\n");
                }

      pthread_mutex_lock(&global_mutex);
      alive = client->alive;
      pthread_mutex_unlock(&global_mutex);
  }
    return (void*) (intptr_t) close(client->connfd);
}

/* signal the global condition variable
 */
void signal_to_bg_task()
{
    pthread_mutex_lock(&global_mutex);
    pthread_cond_broadcast(&global_cond);
    pthread_mutex_unlock(&global_mutex);
}
/* set flags to signal shutdown and
 * signal global condition variable
 */
void server_quit(struct global_state* s)
{
    printf("Quitting\n");
    pthread_mutex_lock(&global_mutex);
    s->quit=1;
    s->running=0;
    pthread_cond_broadcast(&global_cond);
    pthread_mutex_unlock(&global_mutex);
}
void* ui_task(void *ctxt)
{
  struct global_state* s = ctxt;

  while(is_running(s)){
    char c = getchar();
    switch (c){
      case 'q':
	server_quit(s);
        break;
      case 's':
        printf("I could print some status info\n");
        break;
      case 'b':
        signal_to_bg_task();
      default:
#ifdef DEBUG
        printf("ignoring %c\n", c);
#endif
        break;
    }
  }
  return 0;
}

void* bg_task(void *ctxt)
{
  struct global_state* s = ctxt;

  pthread_mutex_lock(&global_mutex);
  while(s->running){
    pthread_cond_wait(&global_cond, &global_mutex);
#ifdef INFO
    printf("bg_task: global_cond was signalled\n");
#endif
  }
  pthread_mutex_unlock(&global_mutex);
  return 0;
}

int serve_clients(struct global_state* state);

void* main_task(void *ctxt)
{
    struct global_state* state = ctxt;
    return (void*) (intptr_t) serve_clients(state);
}


int try_accept(struct global_state* state, struct client* client)
{
    int result = 0;
    if( !state->cam && try_open_camera(state)) {
	printf("Error opening camera\n");
	return 1;
    }
    client->cam = state->cam;
    client->connfd = accept(state->listenfd, (struct sockaddr*)NULL, NULL);
    if(client->connfd < 0) {
        result = errno;
    } else {
#ifdef INFO
	printf("accepted connection\n");
#endif
    	// serve clients in separate thread, for four reasons
    	// 1. Illustrate threading
    	// 2. Not blocking the user interface while serving client
    	// 3. Prepare for serving multiple clients concurrently
        // 4. The AXIS software requires capture to be run outside the main thread

        int statuss = pthread_create(&state->read_thread, 0, read_client, client);
        int status2 = pthread_create(&state->write_thread, 0, serve_client, client);

        if (statuss || status2) { //TODO || pthread_create(&state->write_thread1, 0, read_client, client)
            printf("Error pthread_create()\n");
            perror("creating");
            result = errno;
        } else {
            void* status;
            statuss = pthread_join(state->write_thread, &status);
            status2 = pthread_join(state->read_thread, &status);

            if(status || status2) {
                perror("join");
                result = errno;
            } else {
#ifdef DEBUG
		printf("Join: status = %d\n", (int)(intptr_t) status);
#endif
            }
        }
    }
    return result;
}


static int create_threads(struct global_state* state)
{
    pthread_mutex_lock(&global_mutex);
    int result = 0;
    if (pthread_create(&state->bg_thread, 0, bg_task, state)) {
        printf("Error pthread_create()\n");
        perror("creating bg thread");
        result = errno;
        state->running=0;
        goto failed_to_start_bg_thread;
    }
    if (pthread_create(&state->main_thread, 0, main_task, state)) {
        printf("Error pthread_create()\n");
        perror("creating main thread");
        result = errno;
        state->running=0;
        goto failed_to_start_main_thread;
    }
    if (pthread_create(&state->ui_thread, 0, ui_task, state)) {
        printf("Error pthread_create()\n");
        perror("creating ui thread");
        result = errno;
        state->running=0;
        goto failed_to_start_ui_thread;
    }
    pthread_detach(state->ui_thread);
failed_to_start_ui_thread:
failed_to_start_main_thread:
failed_to_start_bg_thread:
    pthread_mutex_unlock(&global_mutex);
    return result;
}
static void join_bg_thread(pthread_t* bg_thread, const char* msg)
{
    void* status;
    if(pthread_join(*bg_thread, &status)){
        perror("join bg_tread");
    } else {
        printf("Join bg thread (%s): status = " FORMAT_FOR_SIZE_T "\n", msg, (intptr_t) status);
    }
}

static void client_init(struct client* client)
{
    client->cam=NULL;
    client->connfd=-1;
    client->mode = 0;
    client->alive = 1;

}
int serve_clients(struct global_state* state)
{
    int result=0;
    state->pfd.fd = state->listenfd;
    state->pfd.events=POLLIN;
    while(is_running(state)) {
        int ret; // result of poll

        struct client* client = malloc(sizeof(*client));
        if(client == NULL) {
            perror("malloc client");
            result = errno;
            goto failed_to_alloc_client;
    }
	  client_init(client);

	  printf("serve_clients: listening\n");

        do {
            ret = poll(&state->pfd, POLLIN, 2000);
#ifdef DEBUG_VERBOSE
            printf("client poll returns %d\n", ret);
#endif
        } while(ret==0 && is_running(state));

        if(ret <0) {
            perror("poll");
            result = errno;
	          server_quit(state);
        } else if (is_running(state)) {
            if(try_accept(state, client)) {
              perror("try_accept");
              result = errno;
	            server_quit(state);
            };
        }
        free(client);
    }
failed_to_alloc_client:
    close_camera(state);
    return result;
}

void init_global_state(struct global_state* state)
{
    pthread_mutex_lock(&global_mutex);
    state->running=0;
    state->quit=0;
    state->cam=NULL;
    pthread_mutex_unlock(&global_mutex);
}

int is_running(struct global_state* state)
{
    int result=0;
    pthread_mutex_lock(&global_mutex);
    result =  state->running;
    pthread_mutex_unlock(&global_mutex);
    return result;
}
int create_socket(struct global_state* state)
{
    int reuse;
    state->listenfd = socket(AF_INET, SOCK_STREAM, 0);
    state->motionfd = socket(AF_INET, SOCK_STREAM, 0);

    if(state->listenfd < 0 || state->motionfd < 0) {
        perror("socket");
        return errno;
    }

    reuse = 1;
    set_socket_sigpipe_option(state->listenfd);
    if(setsockopt(state->listenfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse))){
      perror("setsockopt listenfd");
      return errno;
    }

    return 0;
}

/* bind state->listenfd to port and listen
 * returns 0 on success
 */
int bind_and_listen(struct global_state* state, int port)
{
    struct sockaddr_in serv_addr;

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);

    if( bind(state->listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr))) {
        perror("bind listenfd");
        return errno;
    }

    if(listen(state->listenfd, 10)){
        perror("listen listenfd");
        return errno;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    int port;
    struct global_state state;
    int result=0;

    if(argc==2) {
        printf("interpreting %s as port number\n", argv[1]);
        port = atoi(argv[1]);
    } else {
        port = 9998;
    }
    printf("starting on port %d\n", port);

    init_global_state(&state);

    if(create_socket(&state)) {
        goto no_server_socket;
    }

    if(bind_and_listen(&state, port)) {
        goto failed_to_listen;
    }
    pthread_mutex_lock(&global_mutex);
    state.running = 1;
    pthread_mutex_unlock(&global_mutex);

    if(create_threads(&state)) {
        goto failed_to_start_threads;
    }

    join_bg_thread(&state.bg_thread, "bg_thread");
    join_bg_thread(&state.main_thread, "main_thread");

failed_to_start_threads:
failed_to_listen:
    if(close(state.listenfd)) perror("closing listenfd");
no_server_socket:
  return result;
}
