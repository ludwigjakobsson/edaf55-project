package client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection {
	private int port;
	private String address;
	private boolean connected;
	private Socket socket;

	public Connection(int port, String address) {
		this.port = port;
		this.address = address;
		
		
	}
	
	public void connect() throws UnknownHostException, IOException {
		this.socket = new Socket(address, port);
		this.connected = true;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void disconnect() throws IOException {
		socket.close();
		connected = false;
	}
	
	
}
