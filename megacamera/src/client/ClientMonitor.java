package client;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import javax.imageio.ImageIO;

public class ClientMonitor {

	private boolean[] shutdown;
	private int currentMode;
	private boolean syncMode;

	private double syncTreshold = 200;
	private long lastImgT[];
	private long lastThreshBreak;
	private int treshBreaks;
	private boolean forcedSyncMode;
	

	private boolean forcedMode;
	private long delay;
	private long lastTs;
	

	private HashMap<Integer, LinkedList<Img>> imageBuffer;
	private HashMap<Integer, LinkedList<String>> messageToServer;

	public ClientMonitor() {

		messageToServer = new HashMap<Integer, LinkedList<String>>();
		imageBuffer = new HashMap<Integer, LinkedList<Img>>();
		for (int i = 0; i < 2; i++) {
			imageBuffer.put(i, new LinkedList<Img>());
			messageToServer.put(i, new LinkedList<String>());
		}
		shutdown = new boolean[2];
		shutdown[0] = false;
		shutdown[1] = false;
 		syncMode = false;
		currentMode = 0;
		treshBreaks = 0;
		forcedSyncMode = false;
		forcedMode = false;
		lastImgT = new long[2];
		lastImgT[0] = System.currentTimeMillis();
		lastImgT[1] = System.currentTimeMillis();

	}

	public synchronized void messageFromServer(long timestamp, int mode, byte[] imageData, int camera) {

		Img img = new Img(timestamp, imageData);
//		long newImgT = Long.parseLong(img.getTime().substring(0, 13));
		
//		if (lastImgT.length != 0) {
//			delay = Math.abs(newImgT - lastImgT[(1-camera)]);
//			if (delay > syncTreshold) {
//				if (treshBreaks > 5) {
//					syncMode = true;
//					treshBreaks = 0;
//					lastThreshBreak = newImgT;
//				} else {
//					treshBreaks++;
//				}
//			} else {
//				treshBreaks = 0;
//			}
//
//		}
//		if (newImgT - lastThreshBreak > 30000) {
//			syncMode = false;
//			//System.out.println("were back biatch!!  :" + newImgT);
//		}
//
//		lastImgT[camera] = newImgT;

		imageBuffer.get(camera).add(img);

		if (!forcedMode)
			currentMode = mode;

		notifyAll();
	}

	public synchronized String waitForMessageToServer(int camera) throws InterruptedException {
		while (messageToServer.get(camera).isEmpty())
			wait();
		String message = messageToServer.get(camera).removeFirst();
		notifyAll();
		return message;
	}

	// gets image
	public synchronized Img getImage(int camera) {
		while (imageBuffer.get(camera).isEmpty()) {
			try {
				wait();
				// System.out.println("waiting for imageBuffer");
			} catch (InterruptedException e) {
				// e.printStackTrace();
			}

		}
		System.out.println("stuck");
		
	
		
		//Nu är vi här för att visa en bild
		//long lastTs = lastImgT[1-camera];
		long lastTime = System.currentTimeMillis();
		if (!imageBuffer.get(camera).isEmpty() && !imageBuffer.get(1-camera).isEmpty()) {
			System.out.println("gaaaay");
			
		long tmp1 = Long.parseLong(imageBuffer.get(camera).peek().getTime().substring(0, 13));
		long tmp2 = Long.parseLong(imageBuffer.get(1-camera).peek().getTime().substring(0, 13));
		System.out.println("gaaaay2");
		long nextTs = tmp1 < tmp2 ? tmp1: tmp2;  
		
		long d = nextTs - lastTs;
		long timeToWaitUntil = lastTime + d;
		
		while (System.currentTimeMillis() < timeToWaitUntil){
			try {
				System.out.println("bruh");
//						
				wait(timeToWaitUntil - System.currentTimeMillis());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		System.out.println("dawdawd");
//		if(newT - lastImgT[1-camera] > syncTreshold) {
//			long sleepT = newT - lastImgT[1-camera];
//		}
		}
		Img temp = imageBuffer.get(camera).removeLast();
		//last shown picture
		long newT = 
		lastImgT[camera] = Long.parseLong(temp.getTime().substring(0, 13));	
		
		lastTs = System.currentTimeMillis();
		return  temp;// Här är vi. Gör något med
														// stackish

	}

	// set the current mode
	public synchronized void setMode(int mode, int camera) {

		if (mode == 2) {
			forcedMode = false;
			messageToServer.get(camera).add("IDLE");
		} else {
			forcedMode = true;
			String message = (mode == 0) ? "IDLE" : "MOVIE";
			messageToServer.get(camera).add(message);

		}
		currentMode = mode;
		notifyAll();
	}

	public boolean isSyncMode() {
		return syncMode;
	}

	// Returns the current mode
	public synchronized int getMode() {
		return currentMode;

	}
	
	public synchronized int getBufferSize(int camera) {
		return imageBuffer.get(camera).size();
	}
	
	public synchronized void clearBuffer(int camera) {
		imageBuffer.get(camera).clear();
	}
	
	public synchronized long getDelay(int camera){
		return lastImgT[camera] - System.currentTimeMillis();
	}


	// Connect Camera
	public synchronized void addConnection(Connection c, int camera) {
		shutdown[camera] = false;

		imageBuffer.put(camera, new LinkedList<Img>());

		new Thread(new ReadThread(this, camera, c)).start();
		new Thread(new WriteThread(this, camera, c)).start();
		new Thread(new ShutdownThread(this, camera, c)).start();

		notifyAll();
	}
	public synchronized void forcedSyncMode() {
		forcedSyncMode =! forcedSyncMode;
	}

	// Disconnect Camera
	
	public synchronized void removeConnection(int camera) {

		shutdown[camera] = true;
		notifyAll();


	}

	public synchronized boolean isShutdown(int camera) {
		return shutdown[camera];
	}

	public synchronized void waitUntilShutdown(int camera) throws InterruptedException {
		while (!shutdown[camera]) {
			wait();
		}

		messageToServer.get(camera).add("SHUTDOWN");
		
		notifyAll();

	}

	public synchronized void waitUntilEmptyMessageBuffer(int camera) throws InterruptedException {
		while (!messageToServer.get(camera).isEmpty())
			wait();
	}

	public synchronized void shutdown(int camera) {
		shutdown[camera] = true;

		notifyAll();

	}

	public synchronized void saveImage(Img img1, Img img2) {
		if (img1 != null) {
			try {
				InputStream in = new ByteArrayInputStream(img1.getData());
				BufferedImage bImage = ImageIO.read(in);
				String dir = "/Users/mattias/edaf55-project/megacamera/src/SavedImages/";
				String time = img1.getTime();

				File outputfile = new File(dir + "camera" + Integer.toString(0) + "date:" + time + ".jpg");
				ImageIO.write(bImage, "jpg", outputfile);
			} catch (IOException e) {

			}
		}

		if (img2 != null) {
			System.out.println("print shit");
			System.out.println("print shit");
			try {
				InputStream in = new ByteArrayInputStream(img2.getData());
				BufferedImage bImage = ImageIO.read(in);
				String dir = "/Users/mattias/edaf55-project/megacamera/src/SavedImages/";
				String time = img2.getTime();

				File outputfile = new File(dir + "camera" + Integer.toString(1) + "date" + time + ".jpg");
				ImageIO.write(bImage, "jpg", outputfile);
			} catch (IOException e) {

			}
		}
	}
}