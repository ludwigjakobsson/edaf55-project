package client;

import java.awt.*;

import javax.swing.*;

/**
 * ImagePanel that contains the pictures
 */
public class ImagePanel extends JPanel  {
	ImageIcon icon;
	private Image image;
	private Img imageData;
	private JLabel label;
	private JLabel delayText;

	private int x;
	private int y;

	public ImagePanel(int x, int y, JLabel delayT) {
		super();
		this.x = x;
		this.y = y;
		this.delayText = delayT;
		initialize();
	}

	private void initialize() {
		this.setLayout(new BorderLayout());
		icon = new ImageIcon();
		label = new JLabel(icon);
		label.setSize(400, 350);
		this.setBackground(Color.BLACK);
		add(label, BorderLayout.CENTER);

	}

	public Image prepare(byte[] data) {

		if (data == null)
			return null;
		Image image = getToolkit().createImage(data);
		Image scaledImage = image.getScaledInstance(x, (int) (0.95 * y), Image.SCALE_SMOOTH);
		getToolkit().prepareImage(scaledImage, -1, -1, null);
		return scaledImage;
	}

	public void refresh(Image image) {
		if (image == null)
			return;
		if (isShowing()) {
			icon.setImage(image);
			icon.paintIcon(this, this.getGraphics(), 0, 0);

		}
	}

	public void changeBackground(Color color) {
		this.setBackground(color);

	}
	public void setText(String text){
		delayText.setText(text);
	}

	public Img getImage() {
		return imageData;
	}

	public void offlinepic() { // Fix later
		FileImage disc = new FileImage("/src/shitImages/disconnect.jpg");
		Image im = prepare(disc.getData());
		refresh(im);

	}
	
}
