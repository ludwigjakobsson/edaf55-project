package client;

import java.awt.Image;

public class GUIData {
	public String delay;
	public Image img;

	public GUIData(String delay, Image img) {
		super();
		this.delay = delay;
		this.img = img;
	}
}
