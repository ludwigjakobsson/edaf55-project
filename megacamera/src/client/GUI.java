package client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;

public class GUI {
	// gui specific stuff
	private JTextField ipField;
	private JTextField portField;
	private JComboBox<String> comboBox;
	private JComboBox<String> modeBox;
	private ImagePanel[] imagePanel;
	private Connection[] connections;
	// monitor related stuff
	private ClientMonitor mon;

	public GUI(ClientMonitor mon) {
		this.mon = mon;
		connections = new Connection[2];
		imagePanel = new ImagePanel[2];
		init();
	}

	public void init() {
		JFrame frame = new JFrame("Mega Awesome Camera UI");
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = (int) (screenDimension.width * 0.8);
		int jFrameHeight = (int) (screenDimension.height * 0.8);
		Container cont = frame.getContentPane();
		cont.setLayout(null);
		//JLayeredPane jpan = new JLayeredPane();
		//jpan.setBounds(0, 0, jFrameWidth, (int) (jFrameWidth * .8));
		//cont.add(jpan);
		frame.setSize(jFrameWidth, jFrameHeight);
		// frame.setLayout(null);
		frame.setResizable(false);
		// frame.setLayout(new LayoutManager());

		frame.getContentPane().setBackground(Color.GRAY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// screenshot button
		JButton screenshot = new JButton("Save Screenshot");
		screenshot.setBounds(20, (int) (frame.getHeight() * 0.8), 150, 50);
		screenshot.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mon.saveImage(imagePanel[0].getImage(), imagePanel[1].getImage()); //CHECK ERORR

			}
		});

		// enter movie mode button
		JButton select_mode = new JButton("Select Mode");
		select_mode.setBounds(200, (int) (frame.getHeight() * 0.8), 150, 50);
		select_mode.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("change mode: " + modeBox.getSelectedIndex());
				System.out.println("cameraindex: " + comboBox.getSelectedIndex());
				mon.setMode(modeBox.getSelectedIndex(), comboBox.getSelectedIndex());
			}
		});

		// exit movie mode button
		JButton exit_movie = new JButton("Toggle Sync");
		exit_movie.setBounds(375, (int) (frame.getHeight() * 0.8), 150, 50);
		exit_movie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Toggle sync mode!");

				mon.forcedSyncMode();

			}
		});

		// Disconnect button
		JButton disconnect = new JButton("Disconnect");
		disconnect.setBounds(frame.getWidth() - 140, (int) (frame.getHeight() * 0.9), 115, 75);
		disconnect.setBackground(Color.RED);
		disconnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				System.out.println("Attempting to disconnect!");
				mon.removeConnection(comboBox.getSelectedIndex());
//				imagePanel[comboBox.getSelectedIndex()].setAlive(false);

			}
		});

		// Connect button
		JButton connect = new JButton("Connect");
		connect.setBounds(frame.getWidth() - 140, (int) (frame.getHeight() * 0.8), 115, 75);
		connect.setBackground(Color.GREEN);
		connect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				String stringPort = portField.getText();

				int intPort = Integer.parseInt(stringPort);

				connections[comboBox.getSelectedIndex()] = new Connection(intPort, ipField.getText());
				try {
					
					
					connections[comboBox.getSelectedIndex()].connect();
					
					System.out.println("Connected! :D");
				} catch (IOException e) {
					System.out.println("Error connecting");
					e.printStackTrace();
				}
				
				if (comboBox.getSelectedIndex() == 0) { // ADD ACTIVE SO NOT CLICK CONNECTA GAN AND CP ERRORRS
				//	Thread t1 = new Thread(imagePanel[comboBox.getSelectedIndex()]);
					ImageThread imageThread1 = new ImageThread(mon, imagePanel[comboBox.getSelectedIndex()], comboBox.getSelectedIndex());
					imageThread1.execute();
				//	imagePanel[comboBox.getSelectedIndex()].setAlive(true);
				//	t1.start();
					mon.addConnection(connections[comboBox.getSelectedIndex()], comboBox.getSelectedIndex());					
				} else {
				//	Thread t2 = new Thread(imagePanel[comboBox.getSelectedIndex()]);
					ImageThread imageThread2 = new ImageThread(mon, imagePanel[comboBox.getSelectedIndex()], comboBox.getSelectedIndex());
					imageThread2.execute();
				//	imagePanel[comboBox.getSelectedIndex()].setAlive(true);
				//	t2.start();
					mon.addConnection(connections[comboBox.getSelectedIndex()], comboBox.getSelectedIndex());
				}

			}
		});

		// textfield for IP
		ipField = new JTextField("Enter IP");
		ipField.setText("argus-8");
		ipField.setBounds((int) (frame.getWidth() * 0.25), (int) (frame.getHeight() * 0.9), 200, 25);
		ipField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				ipField.setText("");
			}

			@Override
			public void focusLost(FocusEvent e) {
			};
		});
		// textfield for port
		portField = new JTextField("Enter Port");
		portField.setText("9999");
		portField.setBounds((int) (frame.getWidth() * 0.05), (int) (frame.getHeight() * 0.9), 200, 25);
		portField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				portField.setText("");
			}

			@Override
			public void focusLost(FocusEvent e) {
			}
		});
		comboBox = new JComboBox<String>();
		comboBox.addItem("Camera 1");
		comboBox.addItem("Camera 2");
		comboBox.setBounds((int) (frame.getWidth() * 0.45), (int) (frame.getHeight() * 0.9), 200, 25);

		modeBox = new JComboBox<String>();
		modeBox.addItem("Idle");
		modeBox.addItem("Movie");
		modeBox.addItem("Auto");
		JLabel delay1 = new JLabel("delay1");
		cont.add(delay1);
		delay1.setBounds((int) (frame.getWidth() * 0.45), (int) (frame.getHeight() * 0.85), 200, 25);
		delay1.setForeground(Color.red);
		JLabel delay2 = new JLabel("delay2");
		delay2.setForeground(Color.red);
		cont.add(delay2);
		delay2.setBounds((int) (frame.getWidth() * 0.65), (int) (frame.getHeight() * 0.85), 200, 25);

		modeBox.setBounds((int) (frame.getWidth() * 0.65), (int) (frame.getHeight() * 0.9), 200, 25);
		imagePanel[0] = new ImagePanel( (frame.getWidth() / 2), (int) (frame.getHeight() * 0.8), delay1);
		imagePanel[0].setBounds(0, 0, (frame.getWidth() / 2), (int) (frame.getHeight() * 0.8));
		imagePanel[1] = new ImagePanel((frame.getWidth() / 2), (int) (frame.getHeight() * 0.8), delay2);
		imagePanel[1].setBounds(frame.getWidth() - (frame.getWidth() / 2), 0, (frame.getWidth() / 2),
				(int) (frame.getHeight() * 0.8));
		// imagePanel2.setBackground(Color.red);
		
		// Add all components to JFrame
		//jpan.add(imagePanel1, 0);
		//jpan.add(imagePanel2, 0);
		 cont.add(imagePanel[0]);
		 cont.add(imagePanel[1]);

		cont.add(screenshot);
		// cont.add(b);
		cont.add(select_mode);
		cont.add(exit_movie);
		cont.add(connect);
		cont.add(disconnect);

		cont.add(ipField);
		cont.add(portField);
		cont.add(comboBox);
		cont.add(modeBox);

		frame.setVisible(true);
		ipField.setVisible(true);
		portField.setVisible(true);

	}

}
