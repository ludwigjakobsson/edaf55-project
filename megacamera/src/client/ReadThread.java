package client;

import java.io.IOException;
import java.io.InputStream;

public class ReadThread extends Thread {


	private ClientMonitor m;
	private int camera;
	private Connection c;
	private int HEADER_LENGTH = 26;

	public ReadThread(ClientMonitor m, int camera, Connection c) {
		this.m = m;
		this.camera = camera;
		this.c = c;


	}

	public void run() {
		
		System.out.println("Started readthread");
		try {
			InputStream in = c.getSocket().getInputStream();
			

			int i = 0;
			while (c.isConnected()) {
				System.out.println("------------------- Image " + ++i + "------------");
			
	
				// Read
			
				System.out.println("Reading Header... ");
				byte[] headerData = readBytes(in, HEADER_LENGTH);
				String headerString = new String(headerData);
				headerString = headerString.replaceAll("\\s+","");
				System.out.println(headerString);
				String[] header = headerString.split("#");
				
				int imageSize = Integer.parseInt(header[0]);
				long timestamp = Long.parseLong(header[1] + header[2]);
				int mode = Integer.parseInt(header[3]);
				
				System.out.println("Image Size: " + imageSize);
				System.out.println("Timestamp: " + timestamp);
				System.out.println("Mode: " + mode);
				
				
				
				//Read Image
				byte[] imageData = readBytes(in, imageSize);
				
				m.messageFromServer(timestamp, mode, imageData, camera);
				
				
				
			}
			
			
		} catch (IOException e) {
			//se.printStackTrace();
		}
		
		System.out.println("Exiting ReadThread " + camera);
	
	}
	
	private static byte[] readBytes(InputStream is, int size) throws IOException {
		byte[] data = new byte[size];
//		int remaining = size;
//		int offset = 0;
//		while (remaining > 0) {
//			int n = is.read(data, offset, remaining);
//			offset += n;
//			remaining -= n;
//			
//		}
		int n;
		int bytesread = 0;
		while((n = is.read(data, bytesread, size-bytesread)) > 0) 
			bytesread += n;

		return data;
	}
	
	
}