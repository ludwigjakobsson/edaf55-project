package client;

import java.io.IOException;
import java.io.OutputStream;

public class WriteThread extends Thread {

	private ClientMonitor m;
	private int camera;
	private Connection c;
	

	public WriteThread(ClientMonitor m, int camera, Connection c) {
		this.m = m;
		this.camera = camera;
		this.c = c;
		

	}

	public void run() {
		System.out.println("Started writethread");
		try {
			
			OutputStream os = c.getSocket().getOutputStream();
			String data;
			byte[] message;

			while(c.isConnected()) {
	
					data = "" + m.waitForMessageToServer(camera);
					System.out.println("Sending " + data);
					message = data.getBytes();
					os.write(message);
					os.flush();
		

			}
			
			

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Exiting ClientWriteThread " + camera);
	}
}
