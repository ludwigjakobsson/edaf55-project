package client;

public class Img {

	private long timestamp;
	private byte[] imageData;

	public Img(long timestamp, byte[] imageData) {
		this.timestamp = timestamp;
		this.imageData = imageData;
	}

	public byte[] getData() {
		return imageData;

	}
	
	public String getTime() {
		return Long.toString(timestamp);
		
	}

}
