package client;

import java.io.IOException;

// This thread has the double responsibility of connecting 
// and reading data from the socket
public class ShutdownThread extends Thread {

	private ClientMonitor monitor;
	private int camera;
	private Connection c;
	
	public ShutdownThread(ClientMonitor mon, int camera, Connection c) {
		monitor = mon;
		this.camera = camera;
		this.c = c;
	}
	
	// Dispose resources upon shutdown
	public void run() {
		try {
			monitor.waitUntilShutdown(camera);
			monitor.waitUntilEmptyMessageBuffer(camera);
			sleep(3000);
			c.disconnect();
		} catch (IOException | InterruptedException e) {
			// Occurs if there is an error in closing the socket.
		}

		System.out.println("Exiting ServerShutdownThread");
	}
}