package client;

import java.awt.Image;
import java.util.List;

import javax.swing.SwingWorker;



public class ImageThread extends SwingWorker<Void, GUIData> {
	private final static int IDLE = 0;
	private final static int MOVIE = 1;
	private final static int AUTO = 2;
	
	private ImagePanel panel;
	private ClientMonitor mon;
	private int camera;
	private long delay;
	private boolean alive;
	private int mode;
	private Img imageData;
	private Image image;
	private String delayText;
	
	public ImageThread(ClientMonitor mon, ImagePanel panel, int camera) {
		this.mon = mon;
		this.panel = panel;
		this.camera = camera;
	}
	
	



	@Override
	protected Void doInBackground() throws Exception {

	
			System.out.println("Started imagePanel");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (!isCancelled()) {
				mode = mon.getMode();
				
				
//				if (mon.isShutdown(camera)) {
//					this.setBackground(Color.black);
//					offlinepic();
//				}
//				if (mon.getBufferSize(camera) > 12) {
//					mon.clearBuffer(camera);
//				}
				switch (mode) {
				case IDLE:
					imageData = mon.getImage(camera);
					
					delay = mon.getDelay(camera);
					//panel.setText("delay:" + delay);
					image = panel.prepare(imageData.getData());
					publish(new GUIData("delay:" + delay, image));
					//panel.refresh(image);
					break;

				case MOVIE:
					if (mon.isSyncMode()) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					imageData = mon.getImage(camera);
					//delayText.setText("<html> delay:<br>" + delay + "</html>");
					delay = mon.getDelay(camera);
					image = panel.prepare(imageData.getData());
					publish(new GUIData("delay:" + delay, image));
					//panel.refresh(image);
					break;

				}

			}

		

	
		return null;
	}
	
	@Override
	protected void process(List<GUIData> chunks) {
		// TODO Auto-generated method stub
		for (GUIData d : chunks){
			panel.refresh(d.img);
			panel.setText(d.delay);
		}
	}
	
	

}
